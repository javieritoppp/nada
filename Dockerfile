FROM ubuntu
MAINTAINER javi <diez.zizurbhi@gmail.com>

ENV PORT 8080
ENV WORKDIR /tmp/server
RUN mkdir ${WORKDIR} && touch ${WORKDIR}/ejemplo.txt
RUN apt-get update && apt-get install
RUN apt-get install -y python

EXPOSE ${PORT}
CMD cd ${WORKDIR} && python -m SimpleHTTPServer ${PORT}